package main

import (
	sm "cloud.google.com/go/secretmanager/apiv1"
	smpb "cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
	"context"
	"fmt"
	"golang.org/x/oauth2"
	"google.golang.org/api/option"
	sts "google.golang.org/api/sts/v1"
	"log"
	"os"
)

const GrantType = "urn:ietf:params:oauth:grant-type:token-exchange"
const RequestedTokenType = "urn:ietf:params:oauth:token-type:access_token"
const SubjectTokenTypeOIDC = "urn:ietf:params:oauth:token-type:id_token"
const GCPAuthScope = "https://www.googleapis.com/auth/cloud-platform"

func main() {
	gcpProjectNumber := os.Getenv("GCP_PROJECT_NUMBER")
	gcpPoolId := os.Getenv("GCP_WIF_POOL_ID")
	gcpProviderId := os.Getenv("GCP_WIF_PROVIDER_ID")
	gcpSecretName := os.Getenv("GCP_SECRET_NAME")

	oidc, err := os.ReadFile("jwt.txt")
	exitWithErr("failed to read oidc tokenResponse file", err)

	// option.WithoutAuthentication() is required for STS service.
	// https://cloud.google.com/iam/docs/reference/sts/rest/v1/TopLevel/token specifies
	// clients NOT to send `Authorization` header. Without the option, the request includes
	// `Authorization` header and the request will fail.
	stsService, err := sts.NewService(context.Background(), option.WithoutAuthentication())

	audience := fmt.Sprintf("//iam.googleapis.com/projects/%s/locations/global/workloadIdentityPools/%s/providers/%s", gcpProjectNumber, gcpPoolId, gcpProviderId)

	req := &sts.GoogleIdentityStsV1ExchangeTokenRequest{
		Audience:           audience,
		GrantType:          GrantType,
		RequestedTokenType: RequestedTokenType,
		Scope:              GCPAuthScope,
		SubjectToken:       string(oidc),
		SubjectTokenType:   SubjectTokenTypeOIDC,
	}

	tokenResponse, err := stsService.V1.Token(req).Do()
	exitWithErr("failed to exchange tokenResponse with google sts", err)

	fmt.Println("GCP access tokenResponse: ", tokenResponse.AccessToken)

	tokenSource := toTokenSource(tokenResponse)

	client, err := sm.NewClient(context.Background(), option.WithTokenSource(tokenSource))
	if err != nil {
		log.Fatalf("failed to create client: %s", err.Error())
	}

	accessSecretVersionReq := &smpb.AccessSecretVersionRequest{
		Name: fmt.Sprintf("projects/%s/secrets/%s/versions/latest", gcpProjectNumber, gcpSecretName),
	}

	accessSecretVersionResponse, err := client.AccessSecretVersion(context.Background(), accessSecretVersionReq)
	if err != nil {
		log.Fatalf("failed to get accessSecretVersionResponse: %v", err)
	}

	fmt.Println("Secret payload: ", string(accessSecretVersionResponse.Payload.Data))
}

func toTokenSource(resp *sts.GoogleIdentityStsV1ExchangeTokenResponse) oauth2.TokenSource {
	return oauth2.StaticTokenSource(&oauth2.Token{
		AccessToken: resp.AccessToken,
		TokenType:   resp.TokenType,
	})
}

func exitWithErr(message string, err error) {
	if err != nil {
		fmt.Printf("%s: %s", message, err)
		os.Exit(1)
	}
}
